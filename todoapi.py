#!flask/bin/python
from flask import Flask, jsonify, abort, make_response, request

app = Flask(__name__)

todos = [
        { 'id': 1, 'sort' : 1, 'text': 'Discuss report with John', 'done': False },
        { 'id': 2, 'sort' : 4, 'text': 'Get a haircut', 'done': True },
        { 'id': 3, 'sort' : 3, 'text': 'Pay electricity bill', 'done': True },
        { 'id': 4, 'sort' : 2, 'text': 'Get gym hours', 'done': False }
]

def get_todo_by_id(todo_id):
    todo = filter(lambda t: t['id'] == todo_id, todos)
    if len(todo) == 0:
        abort(404)
    return todo[0]

def set_todo_done_by_id(todo_id, is_done):
    todo = get_todo_by_id(todo_id=todo_id)
    index = todos.index(todo)
    todo['done'] = False if is_done == 'false' else True
    todos[index] = todo
    return todo

# GET all todos
@app.route('/api/todos', methods=['GET'])
def get_todos():
    return jsonify({'todos': todos})

# GET todo by ID
@app.route('/api/todos/<int:todo_id>', methods=['GET'])
def get_todo(todo_id):
    todo = get_todo_by_id(todo_id=todo_id)
    return jsonify(todo)

# POST new todo
@app.route('/api/todos', methods=['POST'])
def create_todo():
    if not request.json or not 'text' in request.json:
        abort(400)
    todo = {
        'id': todos[-1]['id'] + 1,
        'sort': len(todos) + 1,
        'text': request.json['text'],
        'done': False
    }
    todos.append(todo)
    return jsonify({'todos': todos}), 201

# PUT toggled todo status
@app.route('/api/todos/<int:todo_id>', methods=['PUT'])
def update_todo(todo_id):
    if not request.json or not 'done' in request.json:
        abort(400)
    set_todo_done_by_id(todo_id=todo_id, is_done=request.json['done'])
    return jsonify({'todos' : todos})
    
# DELETE todo
@app.route('/api/todos/<int:todo_id>', methods=['DELETE'])
def delete_todo(todo_id):
	todo = get_todo_by_id(todo_id=todo_id)
	todos.remove(todo)
	return jsonify({'todos': todos}), 204

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

if __name__ == '__main__':
    app.run(debug=True)