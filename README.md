API for https://bitbucket.org/surkova/todos

The application is built with python and Flask, so using `virtualenv` package do:

```
:::bash
$ cd todoapi
```

```
:::bash
$ virtualenv venv
New python executable in venv/bin/python2.7
Also creating executable in venv/bin/python
Installing setuptools, pip...done.
```
```
:::bash
$ . venv/bin/activate
(venv)
```

```
:::bash
$ pip install flask
[...]
Successfully installed flask Werkzeug Jinja2 itsdangerous markupsafe
Cleaning up...
```

```
:::bash
$ python todoapi.py
```